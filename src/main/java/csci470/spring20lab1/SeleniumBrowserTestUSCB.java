package csci470.spring20lab1;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumBrowserTestUSCB {

	public static void main(String[] args) {

		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		WebDriverWait wait = new WebDriverWait(driver, 200);

		try {
			/*
			// First, navigate to USCB main homepage
			driver.get("https://www.uscb.edu");

			// Use the USCB site's search bar to search for "computer science"
			driver.findElement(By.name("q")).sendKeys("computer science" + Keys.ENTER);

			// If proper SEO practices are followed, then USCB's computer science 
			// department should be the first result (but just in case, try to find 
			// the CSS selector that correctly identifies the first non-ad hyperlink 
			// in the search results)
	
			WebElement firstResult = wait.until(
					presenceOfElementLocated(By.cssSelector("div.rc > div.r > a")));
			if ( firstResult.getAttribute("textContent").contains("Computer Science") 
					&& firstResult.getAttribute("textContent").contains("USCB" ) ){
				System.out.println(
						"USCB Computer Science as first web result... Passed.");
			} else {
				System.out.println(
						"USCB Computer Science as first web result... Failed.");
			}
			
			
			// Assuming the link was found, visit the link
			driver.get(firstResult.getAttribute("href"));
			*/
			driver.get("https://www.uscb.edu/cs");
			
			// After the browser has navigated to the above link,
			// validate the page title
			
			if (driver.getTitle().contains("Computer Science")) {
				System.out.println("CS Dept Website Title Test... Passed.");
			} else {
				System.out.println("CS Dept Website Title Test... Failed.");
			}
			

			// Now try the first link under Computer Science in the left menu
			
			WebElement link1 = driver.findElement(
					By.linkText("B.S. in Computational Science"));
			driver.navigate().to(link1.getAttribute("href"));
			
			
			// Validate Page Title
			
			if (driver.getTitle().contains("Bachelor of Science in Computational Science")) {
				System.out.println("B.S. CSCI Website Title Test Passed.");
			} else {
				System.out.println("B.S. CSCI Website Title Test Failed.");
			}
			

			// Simulate "clicking" of the first tab and check to 
			// make sure that the tab's display attribute is visible 
		
			driver.findElement(By.cssSelector("li[rel='tab1']")).click();
			if (driver.findElement(By.cssSelector("div#tab1")).isDisplayed()) {
				System.out.println("B.S. CSCI General Information Tab Test Passed.");
			} else {
				System.out.println("B.S. CSCI General Information Tab Test Failed.");
			}
			

			// On your own: adapt the above code for the second tab
			driver.findElement(By.cssSelector("li[rel='tab2']")).click();
			if (driver.findElement(By.cssSelector("div#tab2")).isDisplayed()) {
				System.out.println("B.S. CSCI Degree Requirements Tab Test Passed.");
			} else {
				System.out.println("B.S. CSCI Degree Requirements Tab Test Failed.");
			}
			

			// On your own: adapt the above code for the third tab
			
			driver.findElement(By.cssSelector("li[rel='tab3']")).click();
			if (driver.findElement(By.cssSelector("div#tab3")).isDisplayed()) {
				System.out.println("B.S. CSCI Course List Tab Test Passed.");
			} else {
				System.out.println("B.S. CSCI Course List Tab Test Failed.");
			}
			

			// On your own: adapt the above code for the fourth tab
			
			driver.findElement(By.cssSelector("li[rel='tab4']")).click();
			if (driver.findElement(By.cssSelector("div#tab4")).isDisplayed()) {
				System.out.println("B.S. CSCI FAQ Tab Test Passed.");
			} else {
				System.out.println("B.S. CSCI FAQ Tab Test Failed.");
			}
			
			
			// On your own: 
			// See if clicking "Apply Now" launches application in new tab
			// Cited reference: https://stackoverflow.com/a/16541981 
			
			String oldTab = driver.getWindowHandle();
			driver.findElement(By.linkText("APPLY NOW")).click();
			ArrayList<String> newTab = 
					new ArrayList<String>(driver.getWindowHandles());
			newTab.remove(oldTab);
			// change focus to new tab
			driver.switchTo().window(newTab.get(0));
			if (driver.getTitle().contains("Admissions Login")) {
				System.out.println(
						"UofSC  Admission Application Launch Test Passed.");
			} else {
				System.out.println(
						"UofSC  Admission Application Launch Test Failed.");
			}
			
			
		} finally {
			// quit the browser when tests have finished or if exception thrown 
			System.out.println("Closing browser...");
			driver.quit();
		}

	} // end method main

} // end class SeleniumBrowserTestUSCB